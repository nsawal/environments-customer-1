#!/bin/bash
#commment
payorUser=weblogic
payorHost=172.29.34.196,172.29.34.197
weblogicDomainDir=/home/weblogic/oracle/wls_12.2.1.2.0/user_projects/domains/uat
weblogicJarsDir=/home/weblogic/oracle/wls_12.2.1.2.0/user_projects/domains/uat/jars
weblogicMappingFile=/home/weblogic/oracle/wls_12.2.1.2.0/user_projects/domains/uat/data/defaultpathnamemapping.txt


export IFS=","
for payorHost in $payorHosts; do

echo "Replacing defaultpathnamemapping on $payorHost"
ssh -q  ${payorUser}@${payorHost} "echo -n >  ${weblogicMappingFile}"
ssh -q  ${payorUser}@${payorHost} "cat > ${weblogicMappingFile} << EOF
Account=Account
Account.retroactiveSubscriptionChangePeriod=AccountretroactiveSubscriptionChangePeriod
Account.effectiveStartDate=AccounteffectiveStartDate
Account.hccIdentifier=AccounthccIdentifier
Account.accountType=AccountaccountType
Account.parentAccount=AccountparentAccount
AccumulatedValuedTransaction=AccumulatedValuedTransaction
AccumulatedValuedTransaction.isCurrent=AccumulatedValuedTransaction.isCurrent
AccumulatedValuedTransaction.lastActivityDate=AccumulatedValuedTransaction.lastActivityDate
AccumulatedValuedTransaction.priorAccumulatorTransactionId=AccumulatedValuedTransaction.priorAccumulatorTransactionId
AccumulatedValuedTransaction.accumulatorAdjustment=AccumulatedValuedTransaction.accumulatorAdjustment
AccumulatedValuedTransaction.claim=AccumulatedValuedTransaction.claim
AccumulatedValueIncrement=AccumulatedValueIncrement
AccumulatedValueIncrement.hccClaimLineNumber=AccumulatedValueIncrement.hccClaimLineNumber
AccumulatedValueIncrement.accumulatorLevel=AccumulatedValueIncrement.accumulatorLevel
AccumulatedValueIncrement.endDate=AccumulatedValueIncrement.endDate
AccumulatedValueIncrement.source=AccumulatedValueIncrement.source
AccumulatedValueIncrement.usedByTransaction=AccumulatedValueIncrement.usedByTransaction
AccumulatedValueIncrement.provisionLabel=AccumulatedValueIncrement.provisionLabel
AccumulatedValueIncrement.accumulatorType=AccumulatedValueIncrement.accumulatorType
AccumulatedValueIncrement.networkTierName=AccumulatedValueIncrement.networkTierName
AccumulatedValueIncrement.lastActivityDate=AccumulatedValueIncrement.lastActivityDate
AccumulatedValueIncrement.networkTierCode=AccumulatedValueIncrement.networkTierCode
AccumulatedValueIncrement.startDate=AccumulatedValueIncrement.startDate
AccumulatedValueIncrement.benefitPlan=AccumulatedValueIncrement.benefitPlan
AccumulatedValueIncrement.member=AccumulatedValueIncrement.member
AccumulatedValueIncrement.accumulatorTransactionId=AccumulatedValueIncrement.accumulatorTransactionId
AccumulatedValueIncrement.subscription=AccumulatedValueIncrement.subscription
AccumulatedValueIncrement.accumulatorDefinition.versionDateQualifier=AccumulatedValueIncrement.accumulatorDefinition.versionDateQualifier
AccumulatedValueIncrement.accumulatorDefinition.conceptTypeName=AccumulatedValueIncrement.accumulatorDefinition.conceptTypeName
AccumulatedValueIncrement.accumulatorDefinition.typeName=AccumulatedValueIncrement.accumulatorDefinition.typeName
AccumulatedValueIncrement.accumulatorDefinition.conceptId=AccumulatedValueIncrement.accumulatorDefinition.conceptId
AccumulatedValueIncrement.accumulatorDefinition.fulfilledCodeQualifier=AccumulatedValueIncrement.accumulatorDefinition.fulfilledCodeQualifier
AccumulatedValueIncrement.accumulatorDefinition.endorsementEffectiveDateQualifier=AccumulatedValueIncrement.accumulatorDefinition.endorsementEffectiveDateQualifier
AccumulatedValueIncrement.accumulatorDefinition.typeId=AccumulatedValueIncrement.accumulatorDefinition.typeId
AccumulatedValueTotal=AccumulatedValueTotal
AccumulatedValueTotal.accumulatorLevel=AccumulatedValueTotal.accumulatorLevel
AccumulatedValueTotal.endDate=AccumulatedValueTotal.endDate
AccumulatedValueTotal.usedAmount=AccumulatedValueTotal.usedAmount
AccumulatedValueTotal.endorsementExpirationDate=AccumulatedValueTotal.endorsementExpirationDate
AccumulatedValueTotal.provisionLabel=AccumulatedValueTotal.provisionLabel
AccumulatedValueTotal.endorsementEffectiveDate=AccumulatedValueTotal.endorsementEffectiveDate
AccumulatedValueTotal.remainingAmount=AccumulatedValueTotal.remainingAmount
AccumulatedValueTotal.definedAmount=AccumulatedValueTotal.definedAmount
AccumulatedValueTotal.accumulatorType=AccumulatedValueTotal.accumulatorType
AccumulatedValueTotal.networkTierName=AccumulatedValueTotal.networkTierName
AccumulatedValueTotal.lastActivityDate=AccumulatedValueTotal.lastActivityDate
AccumulatedValueTotal.networkTierCode=AccumulatedValueTotal.networkTierCode
AccumulatedValueTotal.startDate=AccumulatedValueTotal.startDate
AccumulatedValueTotal.benefitPlan=AccumulatedValueTotal.benefitPlan
AccumulatedValueTotal.member=AccumulatedValueTotal.member
AccumulatedValueTotal.accumulatorTransactionId=AccumulatedValueTotal.accumulatorTransactionId
AccumulatedValueTotal.subscription=AccumulatedValueTotal.subscription
AccumulatedValueTotal.accumulatorDefinition.versionDateQualifier=AccumulatedValueTotal.accumulatorDefinition.versionDateQualifier
AccumulatedValueTotal.accumulatorDefinition.conceptTypeName=AccumulatedValueTotal.accumulatorDefinition.conceptTypeName
AccumulatedValueTotal.accumulatorDefinition.typeName=AccumulatedValueTotal.accumulatorDefinition.typeName
AccumulatedValueTotal.accumulatorDefinition.conceptId=AccumulatedValueTotal.accumulatorDefinition.conceptId
AccumulatedValueTotal.accumulatorDefinition.fulfilledCodeQualifier=AccumulatedValueTotal.accumulatorDefinition.fulfilledCodeQualifier
AccumulatedValueTotal.accumulatorDefinition.endorsementEffectiveDateQualifier=AccumulatedValueTotal.accumulatorDefinition.endorsementEffectiveDateQualifier
AccumulatedValueTotal.accumulatorDefinition.typeId=AccumulatedValueTotal.accumulatorDefinition.typeId
AccumulatorAdjustment=AccumulatorAdjustment
AccumulatorAdjustment.instanceActiveCode=AccumulatorAdjustment.instanceActiveCode
AccumulatorAdjustment.endDate=AccumulatorAdjustment.endDate
AccumulatorAdjustment.description=AccumulatorAdjustment.description
AccumulatorAdjustment.adjustmentDayCount=AccumulatorAdjustment.adjustmentDayCount
AccumulatorAdjustment.provisionLabel=AccumulatorAdjustment.provisionLabel
AccumulatorAdjustment.adjustmentDollarAmount=AccumulatorAdjustment.adjustmentDollarAmount
AccumulatorAdjustment.adjustmentUnitCount=AccumulatorAdjustment.adjustmentUnitCount
AccumulatorAdjustment.startDate=AccumulatorAdjustment.startDate
AccumulatorAdjustment.benefitPlanType=AccumulatorAdjustment.benefitPlanType
AccumulatorAdjustment.adjustmentSource=AccumulatorAdjustment.adjustmentSource
AccumulatorAdjustment.member=AccumulatorAdjustment.member
AccumulatorAdjustment.subscription=AccumulatorAdjustment.subscription
AuditLogEntry=AuditLogEntry
AuditLogEntry.asOfDate=AuditLogEntryASOFDATE
AuditLogEntry.userId=AuditLogUserid
AuditLogEntry.logNote=AuditLogEntrylogNote
AuditLogEntry.entityType=AuditLogentityType
AuditLogEntry.cvcId=AuditLogcvcId
AuditLogEntry.actionQualifier=AuditLogaction
AuditLogEntry.entryReason.codeDomainName=AuditLogEntrycodeDomainName
AuditLogEntry.entryReason.messageCode=AuditLogEntrymessageCode
AuditLogEntry.entryReason.messageDescription=AuditLogEntrymessageDescription
AuditLogEntry.entryReason.policyName=AuditlogentrypolicyName
AuditLogEntry.entryReason.messageAction=AuditLogEntrymessageAction
COBPolicy.benefitPlanType=COBPolicybenefitPlanType
COBPolicy.benefitPlanName=COBPolicybenefitPlanName
COBPolicy.otherInsuranceCompanyName=COBPolicyotherInsuranceCompanyName
COBPolicy.subscriberIdList.effectiveEndDate=COBPolicyEndDate
COBPolicy.subscriberIdList.effectiveStartDate=COBPolicyStartDate
ConsolidatedClaim=ConsolidatedClaim
ConsolidatedClaim.hccClaimNumber=ConsolidatedClaim.hccClaimNumber
ConsolidatedClaim.currentHccClaimNumber=ConsolidatedClaim.currentHccClaimNumber
ConsolidatedClaim.endorsementEffectiveDate=ConsolidatedClaim.endorsementEffectiveDate
ConsolidatedClaim.claimState=ConsolidatedClaim.claimState
ConsolidatedClaim.input=ConsolidatedClaim.input
ConsolidatedClaim.previousConsolidatedClaim=ConsolidatedClaim.previousConsolidatedClaim
ConsolidatedClaim.currentInput=ConsolidatedClaim.currentInput
ConsolidatedClaim.previousClaimState=ConsolidatedClaim.previousClaimState
ConvertedSupplierInvoice=ConvertedSupplierInvoice
ConvertedSupplierInvoice.claimSource=ConvertedSupplierInvoice.claimSource
ConvertedSupplierInvoice.claimPayorType=ConvertedSupplierInvoice.claimPayorType
ConvertedSupplierInvoice.externalClaimNumber=ConvertedSupplierInvoice.externalClaimNumber
ConvertedSupplierInvoice.processingDate=ConvertedSupplierInvoice.processingDate
ConvertedSupplierInvoice.hccClaimNumber=ConvertedSupplierInvoice.hccClaimNumber
CVCInstanceContext=CVCInstanceContext
CVCInstanceContext.latestVersionExpirationDate=CVCInstanceContext.latestVersionExpirationDate
CVCInstanceContext.eventOpsStatus=CVCInstanceContext.eventOpsStatus
CVCInstanceContext.latestIdAsLong=CVCInstanceContext.latestIdAsLong
CVCInstanceContext.latestEndorsementEffectiveDate=CVCInstanceContext.latestEndorsementEffectiveDate
CVCInstanceContext.state=CVCInstanceContext.state
CVCInstanceContext.hccIdentifier=CVCInstanceContext.hccIdentifier
CVCInstanceContext.workbasketAssignmentGroup=CVCInstanceContext.workbasketAssignmentGroup
CVCInstanceContext.prevState=CVCInstanceContext.prevState
CVCInstanceContext.opsName=CVCInstanceContext.opsName
CVCInstanceContext.latestFulfilledCode=CVCInstanceContext.latestFulfilledCode
CVCInstanceContext.latestVersionEffectiveDate=CVCInstanceContext.latestVersionEffectiveDate
CVCInstanceContext.cvcType=CVCInstanceContext.cvcType
CVCInstanceContext.workBasketTransferRuleName=CVCInstanceContext.workBasketTransferRuleName
CVCInstanceContext.latestTypeName=CVCInstanceContext.latestTypeName
CVCInstanceContext.latestIdString=CVCInstanceContext.latestIdString
DentalSupplierInvoice=DentalSupplierInvoice
DentalSupplierInvoice.claimSource=DentalSupplierInvoice.claimSource
DentalSupplierInvoice.claimPayorType=DentalSupplierInvoice.claimPayorType
DentalSupplierInvoice.hccClaimNumber=DentalSupplierInvoice.hccClaimNumber
Issue=Issue
Issue.summary=Issue.summary
Issue.timeSpent=Issue.timeSpent
Issue.description=Issue.description
Issue.reopenDate=Issue.reopenDate
Issue.createdBy=Issue.createdBy
Issue.completionDate=Issue.completionDate
Issue.status=Issue.status
Issue.createDate=Issue.createDate
Issue.requestedCompletionDate=Issue.requestedCompletionDate
Issue.issueType=Issue.issueType
Issue.priority=Issue.priority
Issue.resolution=Issue.resolution
Issue.comments.enteredOn=Issue.comments.enteredOn
Issue.comments.commentText=Issue.comments.commentText
Issue.comments.enteredBy=Issue.comments.enteredBy
ManualReceivable=ManualReceivable
ManualReceivable.doNotOffset=ManualReceivable.doNotOffset
ManualReceivable.amount=ManualReceivable.amount
ManualReceivable.description=ManualReceivable.description
ManualReceivable.payableType=ManualReceivable.payableType
ManualReceivable.commissionType=ManualReceivable.commissionType
ManualReceivable.creditFundingReceivableImmediately=ManualReceivable.creditFundingReceivableImmediately
ManualReceivable.offsetDate=ManualReceivable.offsetDate
ManualReceivable.effectiveDate=ManualReceivable.effectiveDate
ManualReceivable.bankAccount=ManualReceivable.bankAccount
ManualReceivable.product=ManualReceivable.product
ManualReceivable.carrier=ManualReceivable.carrier
ManualReceivable.benefitPlan=ManualReceivable.benefitPlan
ManualReceivable.paymentCategory=ManualReceivable.paymentCategory
ManualReceivable.practitioner=ManualReceivable.practitioner
ManualReceivable.reasonCode=ManualReceivable.reasonCode
ManualReceivable.account=ManualReceivable.account
ManualReceivable.payor.supplierLocation=ManualReceivable.payor.supplierLocation
ManualReceivable.payor.supplier=ManualReceivable.payor.supplier
ManualReceivable.payor.membership=ManualReceivable.payor.membership
ManualReceivable.payor.subscription=ManualReceivable.payor.subscription
ManualReceivable.payor.broker=ManualReceivable.payor.broker
ManualReceivable.payor.account=ManualReceivable.payor.account
ManualReceivable.payor.lienHolder=ManualReceivable.payor.lienHolder
Membership.hccIdentifier=hccIdentifier
Membership.vipReason=MembershipvipReason
Membership.memberRelnshpToSubscriberDef=MbrRelationship
Membership.individual.taxIdentificationNumber=taxIdentificationNumber
Membership.individual.birthDate=birthDate
Membership.individual.dateOfDeath=dateOfDeath
Membership.individual.maritalStatusCode=maritalStatusCode
Membership.individual.genderCode=genderCode
Membership.individual.primaryName.prefixCode=primaryprefix
Membership.individual.primaryName.lastName=primarylastName
Membership.individual.primaryName.firstName=primaryfirstName
Membership.individual.primaryName.middleName=primarymiddleName
Membership.individual.primaryName.nameSuffix.suffixName=primarysuffix
Membership.individual.otherNames.otherNameType=otherType
Membership.individual.otherNames.otherName.middleName=othermiddleName
Membership.individual.otherNames.otherName.lastName=otherlastName
Membership.individual.otherNames.otherName.prefixCode=otherprefix
Membership.individual.otherNames.otherName.firstName=otherfirstName
Membership.individual.otherNames.otherName.nameSuffix.suffixName=othersuffix
Membership.correspondenceAddress.emailAddress=correspondenceemailAddress
Membership.correspondenceAddress.postalAddress.undeliverableAddress=correspondenceundeliverableAddress
Membership.correspondenceAddress.postalAddress.ignoreAddressCheck=correspondenceignoreAddressCheck
Membership.correspondenceAddress.postalAddress.addressType=correspondenceaddressType
Membership.correspondenceAddress.postalAddress.address=correspondenceaddress
Membership.correspondenceAddress.postalAddress.address2=correspondenceaddress2
Membership.correspondenceAddress.postalAddress.address3=correspondenceaddress3
Membership.correspondenceAddress.postalAddress.cityName=correspondencecityName
Membership.correspondenceAddress.postalAddress.stateCode=correspondencestateCode
Membership.correspondenceAddress.postalAddress.countyCode=correspondencecountyCode
Membership.correspondenceAddress.postalAddress.zipCode=correspondencezipCode
Membership.correspondenceAddress.postalAddress.countryCode=correspondencecountryCode
Membership.physicalAddress.emailAddress=physicalemailAddress
Membership.physicalAddress.addressInfo.postalAddress.ignoreAddressCheck=physicalignoreAddressCheck
Membership.physicalAddress.addressInfo.postalAddress.undeliverableAddress=physicalundeliverableAddress
Membership.physicalAddress.addressType=physicalAddressType
Membership.physicalAddress.addressInfo.postalAddress.address=physicalAddress
Membership.physicalAddress.addressInfo.postalAddress.address2=physicalAddress2
Membership.physicalAddress.addressInfo.postalAddress.address3=physicalAddress3
Membership.physicalAddress.addressInfo.postalAddress.cityName=physicalcityName
Membership.physicalAddress.addressInfo.postalAddress.stateCode=physicalstateCode
Membership.physicalAddress.addressInfo.postalAddress.countyCode=physicalcountyCode
Membership.physicalAddress.addressInfo.postalAddress.zipCode=physicalzipCode
Membership.physicalAddress.addressInfo.postalAddress.countryCode=physicalcountryCode
Membership.physicalAddress.addressInfo.addressPhoneList.phoneType=physicalphoneType
Membership.physicalAddress.addressInfo.addressPhoneList.phoneCountryCode=physicalphoneCountryCode
Membership.physicalAddress.addressInfo.addressPhoneList.phoneAreaCode=physicalphoneAreaCode
Membership.physicalAddress.addressInfo.addressPhoneList.phoneNumber=physicalphoneNumber
Membership.physicalAddress.addressInfo.addressPhoneList.phoneExtensionNumber=physicalphoneExt
Membership.memberRepresentative.effectiveEndDate=memberRepresentativeEndDate
Membership.memberRepresentative.hccReviewer=memberRepresentativehccReviewer
Membership.memberRepresentative.scope=memberRepresentativescope
Membership.memberRepresentative.effectiveStartDate=memberRepresentativeStartDate
Membership.memberRepresentative.documentsReceivedDate=memberRepresentativedocumentsReceivedDate
Membership.memberRepresentative.relationshipToMember=memberRepresentativerelationshipToMember
Membership.memberRepresentative.representativeType=memberRepresentativeType
Membership.memberRepresentative.phoneList.phoneAreaCode=memberRepresentativephoneAreaCode
Membership.memberRepresentative.phoneList.phoneNumber=memberRepresentativephoneNumber
Membership.memberRepresentative.phoneList.phoneCountryCode=memberRepresentativephoneCountryCode
Membership.memberRepresentative.phoneList.phoneExtensionNumber=memberRepresentativephoneExtensionNumber
Membership.memberRepresentative.phoneList.phoneType=memberRepresentativephoneType
Membership.memberRepresentative.authorizedPerson.lastName=memberRepresentativeauthorizedPersonlastName
Membership.memberRepresentative.authorizedPerson.firstName=memberRepresentativeauthorizedPersonfirstName
Membership.memberRepresentative.authorizedPerson.middleName=memberRepresentativeauthorizedPersonmiddleName
Membership.memberRepresentative.authorizedPerson.prefixCode=memberRepresentativeauthorizedPersonprefixCode
Membership.memberRepresentative.authorizedPerson.nameSuffix.suffixName=memberRepresentativeauthorizedPersonsuffixName
Membership.otherIdNumberList.effectiveEndDate=otherIdNumberEndDate
Membership.otherIdNumberList.issuingCountry=otherIdNumberissuingCountry
Membership.otherIdNumberList.effectiveStartDate=otherIdNumberStartDate
Membership.otherIdNumberList.identificationNumber=otherIdNumber
Membership.otherIdNumberList.issuingState=otherIdNumberissuingState
Membership.otherIdNumberList.identificationType=otherIdType
MedicareHICNInfo.hicExpirationDate=MedicarehicExpirationDate
MedicareHICNInfo.esrdIndicator=MedicareesrdIndicator
MedicareHICNInfo.workingSpouse=MedicareworkingSpouse
MedicareHICNInfo.hospiceIndicator=MedicarehospiceIndicator
MedicareHICNInfo.hicn=MedicareHICNInfo.hicn
MedicareHICNInfo.hicEffectiveDate=MedicarehicEffectiveDate
MedicareHICNInfo.entryDate=MedicareentryDate
MedicareHICNInfo.dialysisEndDate=MedicaredialysisEndDate
MedicareHICNInfo.waitingPeriodEndDate=MedicarewaitingPeriodEndDate
MedicareHICNInfo.dialysisStartDate=MedicaredialysisStartDate
MedicareHICNInfo.eligibilityReason=MedicareeligibilityReason
MedicareHICNInfo.partADates.terminationDate=MedicarepartAterminationDate
MedicareHICNInfo.partADates.effectiveDate=MedicarepartAeffectiveDate
MedicareHICNInfo.partBDates.terminationDate=MedicarepartBterminationDate
MedicareHICNInfo.partBDates.effectiveDate=MedicarepartBeffectiveDate
MedicareHICNInfo.esrdDateRanges.endDate=MedicareesrdendDate
MedicareHICNInfo.esrdDateRanges.startDate=MedicareesrdstartDate
MemberSelections.manualReview.enabled=manualReviewflag
MemberSelections.manualReview.reason=manualReviewreason
MemberSelections.planSelections.enrolledPlan=MemberenrolledPlan
MemberSelections.planSelections.dateRanges.startDate=MemberPlanstartDate
MemberSelections.planSelections.dateRanges.endDate=MemberPlanendDate
MemberSelections.planSelections.dateRanges.memberTerminateReason=MemberTerminateReason
MemberSelections.planSelections.dateRanges.coverage=MemberPlancoverage
MemberSelections.planSelections.dateRanges.status=MemberPlanstatus
PractitionerRole=PractitionerRole
PractitionerRole.instanceActiveCode=PractitionerRole.instanceActiveCode
PractitionerRole.effectiveStartDate=PractitionerRole.effectiveStartDate
PractitionerRole.roleName=PractitionerRole.roleName
PractitionerRole.supplierLocation=PractitionerRole.supplierLocation
PractitionerRole.practitioner=PractitionerRole.practitioner
PractitionerRole.supplier=PractitionerRole.supplier
PractitionerRole.benefitNetworks=PractitionerRole.benefitNetworks
PractitionerRole.otherIdList.effectiveEndDate=PractitionerRole.otherIdList.effectiveEndDate
PractitionerRole.otherIdList.issuingCountry=PractitionerRole.otherIdList.issuingCountry
PractitionerRole.otherIdList.effectiveStartDate=PractitionerRole.otherIdList.effectiveStartDate
PractitionerRole.otherIdList.identificationNumber=PractitionerRole.otherIdList.identificationNumber
PractitionerRole.otherIdList.issuingState=PractitionerRole.otherIdList.issuingState
PractitionerRole.otherIdList.identificationType=PractitionerRole.otherIdList.identificationType
ReceivedPayment=ReceivedPayment
ReceivedPayment.totalAmount=ReceivedPayment.totalAmount
ReceivedPayment.receiptDate=ReceivedPayment.receiptDate
ReceivedPayment.paymentDate=ReceivedPayment.paymentDate
ReceivedPayment.paymentNumber=ReceivedPayment.paymentNumber
ReceivedPayment.paymentType=ReceivedPayment.paymentType
RecoupmentPayment=RecoupmentPayment
RecoupmentPayment.totalAmount=RecoupmentPayment.totalAmount
RecoupmentPayment.receiptDate=RecoupmentPayment.receiptDate
RecoupmentPayment.paymentDate=RecoupmentPayment.paymentDate
RecoupmentPayment.paymentNumber=RecoupmentPayment.paymentNumber
RecoupmentPayment.paymentType=RecoupmentPayment.paymentType
RecoupmentPayment.payor.supplierLocation=RecoupmentPayment.supplierLocation
RecoupmentPayment.payor.supplier=RecoupmentPayment.supplier
RecoupmentPayment.payor.membership=RecoupmentPayment.membership
RecoupmentPayment.payor.subscription=RecoupmentPayment.subscription
RecoupmentPayment.payor.broker=RecoupmentPayment.broker
RecoupmentPayment.payor.account=RecoupmentPayment.account
RecoupmentPayment.payor.lienHolder=RecoupmentPayment.lienHolder
ReceivableRecoupment=ReceivableRecoupment
ReceivableRecoupment.hccClaimNumber=ReceivableRecoupment.hccClaimNumber
ReceivableRecoupment.amount=ReceivableRecoupment.amount
ReceivableRecoupment.isVoid=ReceivableRecoupment.isVoid
ReceivableRecoupment.isManual=ReceivableRecoupment.isManual
ReceivableRecoupment.recoupmentPayment=ReceivableRecoupment.recoupmentPayment
ReceivableRecoupment.payable=ReceivableRecoupment.payable
ReceivableRecoupment.claim=ReceivableRecoupment.claim
ReceivableRecoupment.receivable=ReceivableRecoupment.receivable
Receivable=Receivable
Receivable.doNotOffset=Receivable.doNotOffset
Receivable.amount=Receivable.amount
Receivable.description=Receivable.description
Receivable.payableType=Receivable.payableType
Receivable.creditFundingReceivableImmediately=Receivable.creditFundingReceivableImmediately
Receivable.offsetDate=Receivable.offsetDate
Receivable.effectiveDate=Receivable.effectiveDate
Receivable.bankAccount=Receivable.bankAccount
Receivable.product=Receivable.product
Receivable.carrier=Receivable.carrier
Receivable.benefitPlan=Receivable.benefitPlan
Receivable.account=Receivable.account
Receivable.payor.supplierLocation=Receivable.payor.supplierLocation
Receivable.payor.supplier=Receivable.payor.supplier
Receivable.payor.membership=Receivable.payor.membership
Receivable.payor.subscription=Receivable.payor.subscription
Receivable.payor.broker=Receivable.payor.broker
Receivable.payor.account=Receivable.payor.account
Receivable.payor.lienHolder=Receivable.payor.lienHolder
Subscription.hccIdentifier=hccIdentifier
Subscription.identifiedAccount=AccountChange
Subscription.vipReason=SubscriptionvipReason
Supplier=Supplier
Supplier.instanceActiveCode=Supplier.instanceActiveCode
Supplier.hccIdentifier=Supplier.hccIdentifier
Supplier.nationalProviderId=Supplier.nationalProviderId
Supplier.effectiveStartDate=Supplier.effectiveStartDate
Supplier.remittanceType=Supplier.remittanceType
Supplier.paymentType=Supplier.paymentType
Supplier.organization.primaryName=Supplier.primaryName
Supplier.organization.taxEntity=Supplier.taxEntity
Supplier.otherCorrespondenceAddressList.emailAddress=Supplier.emailAddress
Supplier.otherCorrespondenceAddressList.correspondenceUsageType=Supplier.correspondenceUsageType
Supplier.otherCorrespondenceAddressList.contactList.emailAddress=Supplier.contactList.emailAddress
Supplier.otherCorrespondenceAddressList.contactList.usageType=Supplier.contactList.usageType
Supplier.otherCorrespondenceAddressList.contactList.personName.lastName=Supplier.contactList.lastName
Supplier.otherCorrespondenceAddressList.contactList.personName.firstName=Supplier.contactList.firstName
Supplier.otherCorrespondenceAddressList.contactList.personName.middleName=Supplier.contactList.middleName
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.zipCode=Supplier.postalAddress.zipCode
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.address=Supplier.postalAddress.address
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.address3=Supplier.postalAddress.address3
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.address2=Supplier.postalAddress.address2
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.addressType=Supplier.postalAddress.addressType
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.countyCode=Supplier.postalAddress.countyCode
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.cityName=Supplier.postalAddress.cityName
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.countryCode=Supplier.postalAddress.countryCode
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.stateCode=Supplier.postalAddress.stateCode
Supplier.otherCorrespondenceAddressList.addressInformation.postalAddress.zipExtensionCode=Supplier.postalAddress.zipExtensionCode
Supplier.otherIdList.effectiveEndDate=Supplier.otherIdList.effectiveEndDate
Supplier.otherIdList.issuingCountry=Supplier.otherIdList.issuingCountry
Supplier.otherIdList.effectiveStartDate=Supplier.otherIdList.effectiveStartDate
Supplier.otherIdList.identificationNumber=Supplier.otherIdList.identificationNumber
Supplier.otherIdList.issuingState=Supplier.otherIdList.issuingState
Supplier.otherIdList.identificationType=Supplier.otherIdList.identificationType
SupplierInvoice=SupplierInvoice
SupplierInvoice.externalCrossReferenceClaimNumber=SupplierInvoice.externalCrossReferenceClaimNumber
SupplierInvoice.claimSource=SupplierInvoice.claimSource
SupplierInvoice.claimPayorType=SupplierInvoice.claimPayorType
SupplierInvoice.externalClaimNumber=SupplierInvoice.externalClaimNumber
SupplierInvoice.hccClaimNumber=SupplierInvoice.hccClaimNumber
SupplierLocation=SupplierLocation
SupplierLocation.nationalProviderId=SupplierLocation.nationalProviderId
SupplierLocation.claimPaymentPayee=SupplierLocation.claimPaymentPayee
SupplierLocation.primaryLocationIndicator=SupplierLocation.primaryLocationIndicator
SupplierLocation.capitatedPaymentPayee=SupplierLocation.capitatedPaymentPayee
SupplierLocation.effectiveStartDate=SupplierLocation.effectiveStartDate
SupplierLocation.isHospital=SupplierLocation.isHospital
SupplierLocation.instanceActiveCode=SupplierLocation.instanceActiveCode
SupplierLocation.locationName=SupplierLocation.locationName
SupplierLocation.hccIdentifier=SupplierLocation.hccIdentifier
SupplierLocation.supplier=SupplierLocation.supplier
SupplierLocation.remittanceType=SupplierLocation.remittanceType
SupplierLocation.paymentType=SupplierLocation.paymentType
SupplierLocation.otherCorrespondenceAddressList.emailAddress=SupplierLocation.emailAddress
SupplierLocation.otherCorrespondenceAddressList.correspondenceUsageType=SupplierLocation.correspondenceUsageType
SupplierLocation.otherCorrespondenceAddressList.contactList.emailAddress=SupplierLocation.contactList.emailAddress
SupplierLocation.otherCorrespondenceAddressList.contactList.usageType=SupplierLocation.contactList.usageType
SupplierLocation.otherCorrespondenceAddressList.contactList.personName.lastName=SupplierLocation.contactList.lastName
SupplierLocation.otherCorrespondenceAddressList.contactList.personName.firstName=SupplierLocation.contactList.firstName
SupplierLocation.otherCorrespondenceAddressList.contactList.personName.middleName=SupplierLocation.contactList.middleName
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.zipCode=SupplierLocation.postalAddress.zipCode
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.address=SupplierLocation.postalAddress.address
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.address3=SupplierLocation.postalAddress.address3
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.address2=SupplierLocation.postalAddress.address2
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.addressType=SupplierLocation.postalAddress.addressType
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.countyCode=SupplierLocation.postalAddress.countyCode
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.cityName=SupplierLocation.postalAddress.cityName
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.countryCode=SupplierLocation.postalAddress.countryCode
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.stateCode=SupplierLocation.postalAddress.stateCode
SupplierLocation.otherCorrespondenceAddressList.addressInformation.postalAddress.zipExtensionCode=SupplierLocation.postalAddress.zipExtensionCode
SupplierLocation.otherIdList.effectiveEndDate=SupplierLocation.otherIdList.effectiveEndDate
SupplierLocation.otherIdList.issuingCountry=SupplierLocation.otherIdList.issuingCountry
SupplierLocation.otherIdList.effectiveStartDate=SupplierLocation.otherIdList.effectiveStartDate
SupplierLocation.otherIdList.identificationNumber=SupplierLocation.otherIdList.identificationNumber
SupplierLocation.otherIdList.issuingState=SupplierLocation.otherIdList.issuingState
SupplierLocation.otherIdList.identificationType=SupplierLocation.otherIdList.identificationType
EOF"

done

exit 0
